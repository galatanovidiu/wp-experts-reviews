<?php
/**
 * Plugin name: RunRepeat Experts Reviews and expert role
 */

/**
 * This will add/edit expert user level when the plugin is activated
 * No deactivation hook will be added to remove the role
 */
register_activation_hook(__FILE__, function () {



    add_role('expert', 'Expert', [
        'edit_expert_review'         => true,
        'read_expert_review'         => true,
        'delete_expert_review'       => true,
        'edit_others_expert_review'  => true,
        'publish_expert_review'      => true,
        'read_private_expert_review' => true,
        'upload_files'               => true,
    ]);

    $administrator     = get_role('administrator');
    foreach ( ['publish','delete','delete_others','delete_private','delete_published','edit','edit_others','edit_private','edit_published','read_private'] as $cap ) {
        $administrator->add_cap( "{$cap}_expert_review" );
    }
});


/**
 * Expert reviews functionality
 */
class ExpertsReviews
{


    public function __construct()
    {
        add_action('init', [$this, 'expert_reviews_post_type']);
        add_filter('pre_get_posts', [$this, 'posts_for_current_author']);
        add_action('restrict_manage_posts', [$this, 'add_author_filter_to_posts_administration']);
        add_action('pre_get_posts', [$this, 'add_author_filter_to_posts_query']);
        add_filter('manage_expert_review_posts_columns', [$this, 'custom_columns']);
        add_filter('wp_dropdown_users_args', [$this, 'wp_dropdown_users_args']);
        add_action('ajax_query_attachments_args', [$this, 'custom_ajax_query_attachments_args']);
        add_action('load-upload.php', [$this, 'only_display_own_uploads']);
        add_action('wp_footer', [$this, 'add_footer_script'], 99);
        add_action('admin_head', [$this, 'admin_head_style'], 99);
        add_action('save_post', [$this, 'save_post_as_pending_review'], 13, 3);
        add_action('post_type_link', [$this, 'post_type_link'], 13, 3);
    }

    public function post_type_link($post_link, $post, $leavename) {
        if ($post->post_type != 'expert_review' || $post->post_status != 'publish') {
            return $post_link;
        }
        $product          = get_post(get_post_meta($post->ID, 'product', true));
        $post_link = str_replace('/.*/', "/" . $product->post_name . "/", $post_link);
        return $post_link;
    }

    /**
     * @param $args
     * @return array
     *
     * Workaround experts not showing on authors lists
     */
    public function wp_dropdown_users_args($args)
    {
        global $post;
        //print_r($post);
        if($args['who'] == 'authors' and $post->post_type == 'expert_review'){
            $args['who'] = '';
        }

        //If current user is expert he can only chose himself as author
        if(current_user_can('expert')){
            $args['include'] =  get_current_user_id();
        }
        //print_r($args);
        return $args;
    }

    /**
     * @param $post_id
     * @param $post
     * @param $update
     * When a new review is added it will be saved as pending review so an admin will decide if a review will be added or not to our system
     */
    public function save_post_as_pending_review($post_id, $post, $update)
    {
        remove_action('save_post', [$this, 'save_post_as_pending_review'], 13, 3); //make sure we do not enter on a infinite loop; reactivate the hook at the end


        $product_id = get_post_meta($post_id, 'product', true);
        if($product_id){
            $product_reviews = get_field('critics', $product_id);
            $author = get_user_by('id', $post->post_author);

            $update = false;

            foreach ($product_reviews as $product_review_key => $product_review) {
                if($product_review['local_expert_id'] == $post->post_author and $post->ID == $product_review['local_expert_review']){

                    $product_reviews[$product_review_key]['site_name'] = 'RunRepeat';
                    $product_reviews[$product_review_key]['site_url'] = get_the_permalink($post_id);
                    $product_reviews[$product_review_key]['critic_name'] = $author->data->display_name;
                    $product_reviews[$product_review_key]['score'] = get_field('score', $post_id);
                    $product_reviews[$product_review_key]['review_content'] = strip_tags(trim(get_field('short_description', $post_id)));
                    $product_reviews[$product_review_key]['youtube_video'] = get_field('youtube_video', $post_id);
                    $product_reviews[$product_review_key]['video_duration'] = get_field('video_duration', $post_id);
                    $product_reviews[$product_review_key]['local_expert_review'] = $post_id;
                    $product_reviews[$product_review_key]['local_expert_id'] = $post->post_author;

                    $update = true;

                }
            }

            if($update == false){

                $existing_critics = 0;
                if(is_array($product_reviews)){
                    $existing_critics = count($product_reviews);
                }
                $product_reviews[$existing_critics]['site_name'] = 'RunRepeat';
                $product_reviews[$existing_critics]['site_url'] = get_the_permalink($post_id);
                $product_reviews[$existing_critics]['critic_name'] = $author->data->display_name;
                $product_reviews[$existing_critics]['score'] = get_field('score', $post_id);
                $product_reviews[$existing_critics]['review_content'] = strip_tags(trim(get_field('short_description', $post_id)));
                $product_reviews[$existing_critics]['youtube_video'] = get_field('youtube_video', $post_id);
                $product_reviews[$existing_critics]['video_duration'] = get_field('video_duration', $post_id);
                $product_reviews[$existing_critics]['local_expert_review'] = $post_id;
                $product_reviews[$existing_critics]['local_expert_id'] = $post->post_author;

            }

            //print_r($product_reviews);exit;

            update_field('critics', $product_reviews, $product_id);
        }


        if (current_user_can('expert') and $post->post_type == 'expert_review' and $update == false) {
            $post->ID          = $post_id;
            $post->post_status = 'pending';

            wp_update_post($post);
        }
        add_action('save_post', [$this, 'save_post_as_pending_review'], 13, 3);
    }

    /**
     * Add some expert related footer scripts
     */
    public function add_footer_script()
    {
        if (current_user_can('expert')) {
            //dashboard link will be changed to experts reviews list
            echo "
        <script>
        jQuery(document).ready(function(){
            jQuery('#wp-admin-bar-site-name a').attr('href', '/wp-admin/edit.php?post_type=expert_review');
        });
        </script>
        ";
        }
    }

    /**
     * hide some sections from admin area for experts only
     */
    public function admin_head_style()
    {
        if (current_user_can('expert')) {
            echo "<style>
            #authordiv, #wpseo_meta, #postcustom, #commentstatusdiv, #commentsdiv, #slugdiv {
            display: none;
            }
            </style>
            ";
        }
    }

    /**
     * To experts only show them the files they have uploaded
     */
    public function only_display_own_uploads()
    {
        if ( ! current_user_can('administrator')) {
            add_action('pre_get_posts', function ($query) {
                $query->set('author', get_current_user_id());
            });
        }
    }


    /**
     * @param $columns
     * @return mixed
     * Add admin column on experts reviews
     */
    public function custom_columns($columns)
    {
        $columns['author'] = 'Author';
        return $columns;
    }


    /**
     * Add Experts reviews custom post type
     * Create experts reviews custom post type
     */

    public function expert_reviews_post_type()
    {

        $labels       = [
            'name'               => _x('Expert Reviews', 'Post Type General Name', 'runrepeat'),
            'singular_name'      => _x('Expert Review', 'Post Type Singular Name', 'runrepeat'),
            'menu_name'          => __('Expert Reviews', 'runrepeat'),
            'parent_item_colon'  => __('Parent Expert Review:', 'runrepeat'),
            'all_items'          => __('All expert reviews', 'runrepeat'),
            'view_item'          => __('View Expert Review', 'runrepeat'),
            'add_new_item'       => __('Add New Expert Review', 'runrepeat'),
            'add_new'            => __('Add New', 'runrepeat'),
            'edit_item'          => __('Edit Expert Review', 'runrepeat'),
            'update_item'        => __('Update Expert Review', 'runrepeat'),
            'search_items'       => __('Search Expert Review', 'runrepeat'),
            'not_found'          => __('Not found', 'runrepeat'),
            'not_found_in_trash' => __('Not found in Trash', 'runrepeat'),
        ];
        $capabilities = array(
            'edit_post'          => 'edit_expert_review',
            'read_post'          => 'read_expert_review',
            'delete_post'        => 'delete_expert_review',
            'edit_posts'         => 'edit_expert_review',
            'edit_others_posts'  => 'edit_others_expert_review',
            'publish_posts'      => 'publish_expert_review',
            'read_private_posts' => 'read_private_expert_review',
        );
        $rewrite = array(
            'slug'                  => '.*/expert',
            'with_front'            => true,
            'pages'                 => true,
            'feeds'                 => true,
        );
        $args         = [
            'label'               => __('expert_review', 'runrepeat'),
            'description'         => __('Expert Reviews pages', 'runrepeat'),
            'labels'              => $labels,
            'supports'            => [
                'title',
                'editor',
                //'excerpt',
                'author',
                'thumbnail',
                //'comments',
                //'trackbacks',
                'revisions',
                'custom-fields',
                //'page-attributes',
            ],
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 20,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            //'capabilities'        => $capabilities,
            'rewrite'               => $rewrite,
        ];
        register_post_type('expert_review', $args);
    }

    /**
     * @param $query
     *
     * @return mixed
     * For experts only show them the reviews they have added
     */
    public function posts_for_current_author($query)
    {
        global $pagenow;
        if ('edit.php' != $pagenow || ! $query->is_admin) {
            return $query;
        }
        if ( ! current_user_can('edit_others_posts')) {
            global $user_ID;
            $query->set('author', $user_ID);
        }
        return $query;
    }

    /**
     * Defining the filter that will be used so we can select posts by 'author'
     */
    public function add_author_filter_to_posts_administration()
    {

        //execute only on the 'expert_review' content type
        global $post_type;
        if ($post_type == 'expert_review' and current_user_can('administrator')) {

            //get a listing of all users that are 'author' or above
            $user_args = array(
                'show_option_all'  => 'All Users',
                'orderby'          => 'display_name',
                'order'            => 'ASC',
                'name'             => 'aurthor_admin_filter',
                'role'             => 'expert',
                'include_selected' => true,
            );

            //determine if we have selected a user to be filtered by already
            if (isset($_GET['aurthor_admin_filter'])) {
                //set the selected value to the value of the author
                $user_args['selected'] = (int)sanitize_text_field($_GET['aurthor_admin_filter']);
            }

            //display the users as a drop down
            wp_dropdown_users($user_args);
        }

    }

    /**
     * @param $query
     * Functionality to filter by user if user filter is set
     */
    public function add_author_filter_to_posts_query($query)
    {

        global $post_type, $pagenow;

        //if we are currently on the edit screen of the post type listings
        if ($pagenow == 'edit.php' && $post_type == 'expert_review') {

            if (isset($_GET['aurthor_admin_filter'])) {

                //set the query variable for 'author' to the desired value
                $author_id = sanitize_text_field($_GET['aurthor_admin_filter']);

                //if the author is not 0 (meaning all)
                if ($author_id != 0) {
                    $query->query_vars['author'] = $author_id;
                }

            }
        }
    }

    /**
     * @param $query
     *
     * @return mixed
     *
     * Media/attachment only display own files for experts
     */
    public function custom_ajax_query_attachments_args($query)
    {
        if ($query['post_type'] == 'attachment' and current_user_can('expert')) {
            $query['author'] = get_current_user_id();
            return $query;
        }

        return $query;
    }


}

new ExpertsReviews();




